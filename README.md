# Bitbucket Pipelines Pipe: AWS ECS deploy

Deploy a containerized application to AWS ECS

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/aws-ecs-deploy:1.1.1
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    CLUSTER_NAME: '<string>'
    SERVICE_NAME: '<string>'
    TASK_DEFINITION: '<string>'
    # WAIT: '<boolean>' # Optional 
    # DEBUG: '<string>' # Optional
```

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**)              | AWS key id. |
| AWS_SECRET_ACCESS_KEY (**) | AWS secret key. |
| AWS_DEFAULT_REGION (**) | AWS region. |
| CLUSTER_NAME (*) | The name of your ECS cluster. |
| SERVICE_NAME (*) | The name of your ECS service. |
| TASK_DEFINITION (*) | Path to the task definition json file. The file should contain a task definition as described in the [AWS docs](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html) |
| WAIT | Wait until the service becomes stable. Default: `false`. The execution will fail if the service doensn't become stable and the timeout of 10 minutes is reached. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


## Prerequisites

To use this pipe you should have an ECS cluster running a service. Learn how to create one [here](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_GetStarted_EC2.html).

## Examples

### Basic example:

```yaml
script:
  - pipe: atlassian/aws-ecs-deploy:1.1.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      CLUSTER_NAME: 'my-ecs-cluster'
      SERVICE_NAME: 'my-ecs-service'
      TASK_DEFINITION: 'task-definition.json'
```

Basic example. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.

```yaml
script:
  - pipe: atlassian/aws-ecs-deploy:1.1.1
    variables:
      CLUSTER_NAME: 'my-ecs-cluster'
      SERVICE_NAME: 'my-ecs-service'
      TASK_DEFINITION: 'task-definition.json'
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2019 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,aws,ecs
